> Pour la realisation des maquettes, j'ai utilisé le logiciel [mockplus](https://mockupplus.en.softonic.com/) qui est en partie libre. Ce logiciel est <b>indispensable</b> pour la visualisation des maquettes.


Le maquettage se deroulera en plusieurs étapes :  
* Prosition de maquettes(minimum 03 modèles) pour chaque partie
* Choix d'une maquette
* Correction de la maquette choisie
* Validation de la maquette choisie 


## Partie 1 : Ecran de connexion / Ecran de creation du compte

### 1. Ecran de connexion

Dispose de :  
* deux (02) champs de texte :  
Le nom d'utilisateur (login)  
Son mot de passe (password)

* un bouton de connexion au compte (Sign In)

* un lien de sécours en cas d'oubli du mot de passe (forgot your password ?)

* un lien vers l'ecran de creation du compte (Sign Up) pour les nouveaux utilisateurs

### 2. Ecran de creation du compte
Dispose de : 
* six (06) champs de texte :  
Le prenom de l'utilisateur (firstname)  
Le nom de famille de l'utilisateur (lastname)  
Son nom d'utilisateur (login)  
Son email 
Son mot de passe
Et la confirmation du mot de passe  

* un bouton de validation de la creation du compte (Sign Up)

* un champ image pour la photo de profil 

* un lien vers l'ecran de connexion(Sign In) pour les utilisateurs possedant déja un compte  

> Choix du thème ?????
